class amanda::client (
    String $amanda_package,
    Array[Hash] $exclude_list,
) inherits amanda {

    #$my_dle = hiera('amanda::dle')
    #notify{"amanda::client debug1": message => $my_dle}
    if ! empty(grep(hiera('amanda::dle').keys, $hostname)) {
	$enabled = true
    }

#    if ($enabled) {
#	notify{"amanda::client is enabled": }
#    } else {
#	notify{"amanda::client is disabled": }
#    }

    package {"$amanda_package":
        ensure => ($enabled)?{true=>'installed', false=>'absent'},
    }

    if $enabled {
	file { "$::amanda::homedir/.ssh":
	    ensure => directory,
	    owner => $::amanda::user,
	    group => $::amanda::group,
	    mode => $::amanda::homedirmode,
	    require => Package["$amanda_package"],
	}
	file { "$::amanda::homedir/.ssh/authorized_keys":
	    ensure => file,
	    owner => $::amanda::user,
	    group => $::amanda::group,
	    mode => '0644',
	    content => epp('amanda/authorized_keys', {
		'pubkeys' => $::amanda::pubkeys,
	    }),
	}
    }

    $exclude_list.each |$exclude_hash| {
	file {$exclude_hash[path]:
	    ensure => file,
	    owner => $::amanda::user,
	    group => $::amanda::group,
	    mode => '0644',
	    content => epp('amanda/exclude_list.epp', {
		'dirs' => $exclude_hash[dirs],
	    }),
	}
    }

    $::amanda::profiles.each |$name, $profile| {
	$profile_config_path="/etc/amanda/${name}/amanda-client.conf"
	# The gnutar lists files reside in a subdir at the top of the amanda
	# backup user's home directory, unless otherwise specified.
	if ($profile['gnutar_list_basedir']) {
	    $gnutar_list_basedir = $profile['gnutar_list_basedir']
	} else {
	    $gnutar_list_basedir = "$::amanda::homedir"
	}
	file {"/etc/amanda/${name}/amanda-client.conf":
	    ensure => file,
	    owner => $::amanda::user,
	    group => $::amanda::group,
	    mode => '0644',
	    content => epp('amanda/amanda-client.conf', {
		'confname' => $name,
		'index_server' => $profile['index_server'],
		'tape_server' => $profile['tape_server'],
		'unreserved_tcp_port' => $profile['unreserved_tcp_port'],
		'gnutar_list_basedir' => $gnutar_list_basedir,
	    }),
	}
    }
}
