class amanda (
    String $homedir,
    String $homedirmode,
    String $user,
    String $group,
    Array[String] $pubkeys,
    Hash[String,Hash] $profiles,
) {
    $profiles.each |$name, $profile| {
	Package <| |> ->
	file {"/etc/amanda/${name}":
	    ensure => directory,
	    owner => $::amanda::user,
	    group => $::amanda::group,
	    mode => '0644',
	}
    }
}
