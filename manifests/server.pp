class amanda::server (
    String $amanda_server_version = 'installed',
) inherits amanda {
    package {'amanda-server':
        ensure => $::amanda::server::version,
    }
    package {'mt-st': ensure => installed, }
    package {'mtx': ensure => installed, }
    package {'perl-JSON': ensure => installed, }

    $::amanda::profiles.each |$name, $profile| {
        file {"/etc/amanda/${name}/disklist":
            ensure  => file,
            owner   => $::amanda::user,
            group   => $::amanda::group,
            mode    => '0644',
            content => epp('amanda/disklist.epp', {
                'dle' => lookup('amanda::dle'),
            }),
        }
    }
}
